import numpy as np
import math

def get_bloated_point(point, delta):
    return np.row_stack((point - delta, point + delta)).tolist()

def get_guards(waypoints, delta):
    return str(list(map(lambda x: ["Box", get_bloated_point(x, delta)], waypoints[1:]))).replace("'", '"')

def get_waypoints(waypoints):
    return str(list(map(lambda x: ["follow_waypoint", x.tolist()], waypoints[1:]))).replace("'", '"')

def get_initset(waypoints, delta):
    return str(list(map(lambda x: ["Box", get_bloated_point(x, delta)], waypoints[0:1]))[0]).replace("'", '"')

def parse_waypoints(waypoints, add_psi_c=False):
    if waypoints[0][0] == "follow_waypoint":
        waypoints = list(map(lambda x: x[1], waypoints))
    if add_psi_c:
        waypoints[0].append(0)
        waypoints = [waypoints[0]] + [[new[0], new[1], math.atan2(new[0]-old[0], new[1]-old[1])] for old, new in zip(waypoints[:-1], waypoints[1:])]
    return waypoints

def rotate_2D_waypoints(waypoint: np.array, theta, x_ind, y_ind):
    # build rotation matrix
    rot_mat = np.eye(waypoint.shape[0])
    rot_mat[x_ind, x_ind] = math.cos(theta)
    rot_mat[y_ind, y_ind] = math.cos(theta)
    rot_mat[y_ind, x_ind] = math.sin(theta)
    rot_mat[x_ind, y_ind] = -1 * math.sin(theta)
    return rot_mat @ waypoint

def main():
    theta: float = 0 # math.pi / 3
    x_ind: int = 0
    y_ind: int = 1
    translation_vec = np.array([3.0, -3.0, -11.0], dtype=float)
    waypoints = [["follow_waypoint", [-5, -5, 5]], ["follow_waypoint", [5, -5, 5]], ["follow_waypoint", [5, 0, 5]], ["follow_waypoint", [-5, 0, 5]], ["follow_waypoint", [-5, 5, 5]], ["follow_waypoint", [5, 5, 5]]]
    waypoints = parse_waypoints(waypoints)
    guard_delta = np.array([0.5, 0.5, 0.5])
    initset_delta = np.array([0.5, 0.5, 0.5])
    waypoints = list(map(np.array, waypoints))
    if not np.array_equal(translation_vec, np.zeros(waypoints[0].shape, dtype=float)):
        waypoints = list(map(lambda x: x + translation_vec, waypoints))
    if theta != 0.0:
        waypoints = list(map(lambda x: rotate_2D_waypoints(x, theta, x_ind, y_ind), waypoints))
    print('"mode_list":', get_waypoints(waypoints) + ',')
    print('"guards":', get_guards(waypoints, guard_delta) + ',')
    print('"initialSet":', get_initset(waypoints, initset_delta))


if __name__ == '__main__':
    # print(rotate_2D_waypoints(np.array([1, 1]), math.pi/18, 0, 1))
    main()