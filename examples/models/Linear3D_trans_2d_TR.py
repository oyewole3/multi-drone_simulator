from scipy.integrate import odeint
import numpy as np
import math as m
from typing import Optional, List, Tuple
from src.Waypoint import Waypoint
import polytope as pc
from src.PolyUtils import PolyUtils

A = [[-3.0, 0.0, 0.0], [0.0, -3.0, 0.0], [0.0, 0.0, -1.0]]


def truncate(n):
    return int(n * 1) / 1


# Dynamics of the linear system
def linear_dynamic(x, t, x_d):
    dvdt = np.dot(np.array(A), x) - np.dot(np.array(A), np.array(x_d))
    return dvdt


def simulation_single(system_dynamics, initial_state, time_step, time_bound, x_d):
    time_bound = float(time_bound)
    time_step = float(time_step)
    number_points = int(np.ceil(time_bound / time_step))
    t = [i * time_step for i in range(0, number_points)]
    if t[-1] != time_step:
        t.append(time_bound)
    newt = []
    for step in t:
        newt.append(float(format(step, '.4f')))
    t = np.array(newt)

    sol = odeint(system_dynamics, initial_state, t, args=(x_d,), hmax=time_step)

    # Construct the final output
    trace = np.column_stack((t, sol[:, :3]))
    return trace


def TC_Simulate(mode: str, mode_parameters: Optional[List[float]], time_bound: float, time_step: float,
                initial_point: np.array) -> np.array:
    if mode == 'follow_waypoint':
        assert isinstance(mode_parameters, list) and (len(mode_parameters) == 3), "must give length 3 list as params to follow_waypoint mode of 3D follower"
        trace = simulation_single(linear_dynamic, initial_point, time_step, time_bound, mode_parameters)
        return trace
    else:
        raise ValueError("Mode: ", mode, "is not defined for the 3D linear follower")


"""
def get_transform_information(mode_parameters: List[float], initset: pc.Polytope, mode: str ="follow_waypoint") -> Tuple[np.array, float]:
    if mode != "follow_waypoint":
        raise NotImplementedError("haven't implemented modes other than follow waypoint for these linear dynamics")
    old_center = np.average(PolyUtils.get_bounding_box(initset), axis=0)
    dot = (mode_parameters[1] - old_center[1])  # dot product between [x1, y1] and [x2, y2]
    det = (mode_parameters[0] - old_center[0])  # determinant
    dir_angle = m.atan2(det, dot)
    translation_vector = -1 * np.array(mode_parameters)
    return translation_vector, dir_angle
"""


def get_transform_information(mode_parameters: List[float], prev_mode_parameters: List[float],
                              mode: str = "follow_waypoint") -> Tuple[np.array, float]:
    if mode != "follow_waypoint":
        raise NotImplementedError("haven't implemented modes other than follow waypoint for these linear dynamics")
    old_center = prev_mode_parameters
    dot = (mode_parameters[1] - old_center[1])  # dot product between [x1, y1] and [x2, y2]
    det = (mode_parameters[0] - old_center[0])  # determinant
    dir_angle = m.atan2(det, dot)
    translation_vector = -1 * np.array(mode_parameters)
    return translation_vector, dir_angle


def transform_poly_to_virtual(poly, transform_information):
    translation_vector, new_system_angle = transform_information
    poly_out: pc.Polytope = poly.translation(translation_vector)
    return poly_out.rotation(i=0, j=1, theta=new_system_angle) # new_system_angle


def transform_mode_to_virtual(point, transform_information):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    z1 = point[2]
    translation_vector, sc = transform_information
    x_n = translation_vector[0]
    y_n = translation_vector[1]
    x2 = (x1 + x_n) * m.cos(sc) - (y1 + y_n) * m.sin(sc)
    y2 = (x1 + x_n) * m.sin(sc) + (y1 + y_n) * m.cos(sc)
    # x2 = x1 + x_n
    # y2 = y1 + y_n
    x2 = round(x2)
    y2 = round(y2)
    z1 = round(z1)
    return [x2, y2, z1]


def transform_poly_from_virtual(poly, transform_information):
    new_system_angle = -1 * transform_information[1]
    translation_vector = -1 * transform_information[0]
    out_poly: pc.Polytope = poly.rotation(i=0, j=1, theta=new_system_angle) # new_system_angle
    return out_poly.translation(translation_vector)


def transform_mode_from_virtual(point, transform_information):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    z1 = point[2]
    translation_vector, sc = transform_information
    x_n = translation_vector[0]
    y_n = translation_vector[1]
    x2 = x1 * m.cos(sc) + y1 * m.sin(sc) - x_n
    y2 = -1 * x1 * m.sin(sc) + y1 * m.cos(sc) - y_n
    # x2 = x1 - x_n
    # y2 = y1 - y_n
    return [x2, y2, z1]


def get_virtual_mode_parameters():
    return [0.0, 0.0, 0.0]


def get_flowstar_parameters(mode_parameters: List[float], initial_set: np.array, time_step: float, time_bound: float,
                            mode: str):
    if mode != "follow_waypoint":
        raise NotImplementedError("These linear dynamics only support waypoint following mode")
    num_vars = len(A) + 1
    order = 4
    hyper_params = [str(num_vars), str(time_step), str(time_bound), str(order)]
    cur_list: List[str] = hyper_params[:]
    var_names: List[str] = ["t", "x", "y", "z"]
    cur_list.extend(var_names[:num_vars])
    ode_rhs = ["1"]
    ode_rhs.extend([' + '.join([str(val) + " * (" + var_names[ind + 1] + ' - ' + "(" + str(mode_parameters[ind]) + "))"
                                for ind, val in enumerate(A_row)]) for A_row in A])
    cur_list.extend(ode_rhs)
    # time initset lowerbound
    cur_list.append('0')
    cur_list.extend([str(val) for val in initial_set[0, :]])
    # time initset upperbound
    cur_list.append('0')
    cur_list.extend([str(val) for val in initial_set[1, :]])
    return cur_list


def transform_state_from_then_to_virtual_dryvr_string(point, transform_information_from, transform_information_to):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    z1 = point[2]
    translation_vector_from, dir_angle_from = transform_information_from
    translation_vector_to, dir_angle_to = transform_information_to
    x_n_from = translation_vector_from[0]
    y_n_from = translation_vector_from[1]
    z_n_from = translation_vector_from[2]
    x2 = x1 * m.cos(dir_angle_from) + y1 * m.sin(dir_angle_from) - x_n_from
    y2 = -1 * x1 * m.sin(dir_angle_from) + y1 * m.cos(dir_angle_from) - y_n_from
    x_n_to = translation_vector_to[0]
    y_n_to = translation_vector_to[1]
    z_n_to = translation_vector_to[2]
    x3 = (x2 + x_n_to) * m.cos(dir_angle_to) - (y2 + y_n_to) * m.sin(dir_angle_to)
    y3 = (x2 + x_n_to) * m.sin(dir_angle_to) + (y2 + y_n_to) * m.cos(dir_angle_to)
    x_reset_string = " x * cos(" + str(dir_angle_from) + ")" + "+ y * sin(" + str(dir_angle_from) + ") -1 * (" + str(x_n_from) + ")"
    y_reset_string = "-1 * x * sin(" + str(dir_angle_from) + ")" + "+ y * cos(" + str(dir_angle_from) + ") -1 * ( " + str(y_n_from)+ ")"
    x_reset_string_2 = "x = (" + x_reset_string + " + " + str(x_n_to) + ")" + "* cos(" + str(dir_angle_to) + ") - ( " \
                     + str(y_reset_string) + " + " + str(y_n_to) + " ) * sin(" + str(dir_angle_to) + " )"
    y_reset_string_2 = "y = (" + x_reset_string + " + " + str(x_n_to) + ")" + "* sin(" + str(dir_angle_to) + ") + ( " \
                     + str(y_reset_string) + " + " + str(y_n_to) + " ) * cos(" + str(dir_angle_to) + " )"
    z_reset_string_2 = "z = z + -1 * (" + str(z_n_from) + ") + " + str(z_n_to)
    return x_reset_string_2 + ";" + y_reset_string_2 + "; " + z_reset_string_2


if __name__ == '__main__':
    print('"' + '" "'.join(
        get_flowstar_parameters([5, 5, 5], np.array([[0, 0, 0], [1, 1, 1]]), 0.05, 5, "follow_waypoint")) + '"')
