from scipy.integrate import odeint
import numpy as np
import math as m
from typing import Optional, List, Tuple

A = [[-3.0, 0.0, 0.0], [0.0, -3.0, 0.0], [0.0, 0.0, -1.0]]


def truncate(n):
    return int(n * 1) / 1


# Dynamics of the linear system
def linear_dynamic(x, t, x_d):
    dvdt = np.dot(np.array(A), x) - np.dot(np.array(A), np.array(x_d))
    return dvdt


def simulation_single(system_dynamics, initial_state, time_step, time_bound, x_d):
    time_bound = float(time_bound)
    time_step = float(time_step)
    number_points = int(np.ceil(time_bound / time_step))
    t = [i * time_step for i in range(0, number_points)]
    if t[-1] != time_step:
        t.append(time_bound)
    newt = []
    for step in t:
        newt.append(float(format(step, '.4f')))
    t = np.array(newt)

    sol = odeint(system_dynamics, initial_state, t, args=(x_d,), hmax=time_step)

    # Construct the final output
    trace = np.column_stack((t, sol[:, :3]))
    return trace


def TC_Simulate(mode: str, mode_parameters: Optional[List[float]], time_bound: float, time_step: float,
                initial_point: np.array) -> np.array:
    if mode == 'follow_waypoint':
        assert isinstance(mode_parameters, list) and (len(mode_parameters) == 3), "must give length 3 list as params to follow_waypoint mode of 3D follower"
        trace = simulation_single(linear_dynamic, initial_point, time_step, time_bound, mode_parameters)
        return trace
    else:
        raise ValueError("Mode: ", mode, "is not defined for the 3D linear follower")


if __name__ == '__main__':
    trace = TC_Simulate("follow_waypoint", [3,3,3], 10, 0.1, [0,0,0])
    print("trace: ", trace)
