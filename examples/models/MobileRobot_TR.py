# MobileRobot
from scipy.integrate import odeint
import numpy as np
import math
from typing import Optional, List, Tuple
import polytope as pc
from src.PolyUtils import PolyUtils


def position(q: np.array, t: float, vc: float, w: float,x_n, y_n, k) -> np.array:
    x = q[0]
    y = q[1]
    theta = q[2]

    v = vc

    x_ref = x_n
    y_ref = y_n
    L = k

    alpha = math.atan2((y_ref - y),(x_ref - x)) - theta
    dthetadt = 2*v*math.sin(alpha)/L

    dxdt = v*np.cos(theta)
    dydt = v*np.sin(theta)

    return np.array((dxdt, dydt, dthetadt))


# function to provide traces of the system
def TC_Simulate(mode: str, mode_parameters: Optional[List[float]], time_bound: float, time_step: float,
                initial_point: np.array) -> np.array:
    if mode == 'follow_waypoint':
        # mode parameters for this is the waypoint center
        t = np.arange(0, time_bound + time_step, time_step)
        assert isinstance(mode_parameters, list) and (len(mode_parameters) == 2) and (isinstance(
            mode_parameters[0], float) or isinstance(
            mode_parameters[0], int)), "must give length 2 list as params to follow_waypoint mode of Mobile Robot"
        red_args = (1, 0, mode_parameters[0], mode_parameters[1], 1)
        sol = odeint(position, initial_point, t, args=red_args, hmax=time_step)
        sol = [[x[0],x[1],x[2] % (2 * math.pi)] for x in sol]
        new_sol = []
        for x in sol:
            while x[2] > math.pi:
                x[2] = x[2] - 2 * math.pi
            while x[2] < -math.pi:
                x[2] = x[2] + 2 * math.pi
            new_sol.append(x)
        sol = [[x[0],x[1],x[2] % (2 * math.pi)] for x in sol]
        trace = np.column_stack((t, sol))
        return trace
    else:
        raise ValueError("Mode: ", mode, "is not defined for the Mobile Robot")


def get_transform_information(mode_parameters: List[float], prev_mode_parameters: List[float],
                              mode: str = "follow_waypoint") -> Tuple[np.array, float]:
    if mode != "follow_waypoint":
        raise NotImplementedError("haven't implemented modes other than follow waypoint for these linear dynamics")
    old_center = prev_mode_parameters
    #old_center = np.average(PolyUtils.get_bounding_box(initset), axis=0)
    dot = (mode_parameters[1] - old_center[1])  # dot product between [x1, y1] and [x2, y2]
    det = (mode_parameters[0] - old_center[0])  # determinant
    dir_angle = math.atan2(det, dot)
    translation_vector: np.array = np.zeros((3,))
    translation_vector[:2] = -1 * np.array(mode_parameters)
    translation_vector[2] = -1 * dir_angle
    # print(translation_vector, dir_angle)
    return translation_vector, dir_angle


def transform_poly_to_virtual(poly, transform_information):
    translation_vector, new_system_angle = transform_information
    # print(translation_vector)
    # print(poly)
    poly_out: pc.Polytope = poly.translation(translation_vector)
    return poly_out.rotation(i=0, j=1, theta=new_system_angle)

def transform_mode_to_virtual(point, transform_information):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    translation_vector, sc = transform_information
    x_n = translation_vector[0]
    y_n = translation_vector[1]
    x2 = (x1 + x_n) * math.cos(sc) - (y1 + y_n) * math.sin(sc)
    y2 = (x1 + x_n) * math.sin(sc) + (y1 + y_n) * math.cos(sc)
    #x2 = x1 + x_n
    #y2 = y1 + y_n
    x2 = round(x2)
    y2 = round(y2)
    return [x2, y2]

def transform_poly_from_virtual(poly, transform_information):
    new_system_angle = -1 * transform_information[1]
    translation_vector = -1 * transform_information[0]
    out_poly: pc.Polytope = poly.rotation(i=0, j=1, theta=new_system_angle)
    return out_poly.translation(translation_vector)



def transform_mode_from_virtual(point, transform_information):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    translation_vector, sc = transform_information
    x_n = translation_vector[0]
    y_n = translation_vector[1]
    x2 = x1 * math.cos(sc) + y1 * math.sin(sc) - x_n
    y2 = -1 * x1 * math.sin(sc) + y1 * math.cos(sc) - y_n
    #x2 = x1 - x_n
    #y2 = y1 - y_n
    return [x2, y2]

# Copied from Linear3D_trans_2d
def transform_state_from_then_to_virtual_dryvr_string(point, transform_information_from, transform_information_to):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    z1 = point[2]
    translation_vector_from, dir_angle_from = transform_information_from
    translation_vector_to, dir_angle_to = transform_information_to
    x_n_from = translation_vector_from[0]
    y_n_from = translation_vector_from[1]
    z_n_from = translation_vector_from[2]
    x2 = x1 - x_n_from
    y2 = x1 - y_n_from
    x_n_to = translation_vector_to[0]
    y_n_to = translation_vector_to[1]
    z_n_to = translation_vector_to[2]
    x3 = (x2 + x_n_to)
    y3 = (y2 + x_n_to)
    x_reset_string = "x " + " -1 * (" + str(x_n_from) + ")"
    y_reset_string = "x " + "-1 * ( " + str(y_n_from)+ ")"
    x_reset_string_2 = "x = (" + x_reset_string + " + " + str(x_n_to) + ")"
    y_reset_string_2 = "y = (" + y_reset_string + " + " + str(y_n_to) + ")"
    z_reset_string_2 = "z = z -1 * (" + str(z_n_from) + ") + " + str(z_n_to)
    return x_reset_string_2 + ";" + y_reset_string_2 + "; " + z_reset_string_2

    
def get_virtual_mode_parameters():
    return [0.0, 0.0]

def get_flowstar_parameters():
    pass