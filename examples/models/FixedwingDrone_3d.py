from scipy.integrate import odeint
import numpy as np
import math
from typing import Optional, List, Tuple
import polytope as pc
from src.PolyUtils import PolyUtils


# function to return derivatives of state to be integrated
def position(p: np.array, t: float, cd1: float, vc: float, m: float,
             k1: float, k2: float, k3: float, x_n: float, y_n: float, z_n: float) -> np.array:
    # get initial conditions
    if p.shape != (5,):
        print("bad shape:", p.shape)
        raise ValueError("p must be a length 4 array for drone")

    x: float = p[0]  # x_i
    y: float = p[1]  # y_i
    z: float = p[2]
    s: float = p[3]  # psi
    v: float = p[4]  # velocity
    # constants
    G: float = 32.2  # ft/sec

    # compute dvdt
    # vc = math.sqrt((x - x_n)*(x - x_n)+(y - y_n)*(y - y_n)) / ts
    D: float = cd1 * v ** 2  # drag
    T: float = k1 * m * (vc - v)  # thrust
    dvdt: float = (T - D) / m  # v'

    dot: float = (y_n - y)  # dot product between [x1, y1] and [x2, y2]
    det: float = (x_n - x)  # determinant
    sh: float = math.atan2(det, dot)  # atan2(y, x) or atan2(sin, cos)
    h: float = (k2 * vc / G) * (sh - s)
    dsdt: float = (G / v) * np.sin(h)  # * (sh - s) #

    dxdt: float = v * np.sin(s)
    dydt: float = v * np.cos(s)
    dzdt: float = k3 * (z_n - z)
    return np.array((dxdt, dydt, dzdt, dsdt, dvdt))


# function to provide traces of the system
def TC_Simulate(mode: str, mode_parameters: Optional[List[float]], time_bound: float, time_step: float,
                initial_point: np.array) -> np.array:
    if mode == 'follow_waypoint':
        # mode parameters for this is the waypoint center
        t = np.arange(0, time_bound + time_step, time_step)
        assert isinstance(mode_parameters, list) and (len(mode_parameters) == 3) and (isinstance(
            mode_parameters[0], float) or isinstance(
            mode_parameters[0], int)), "must give length 2 list as params to follow_waypoint mode of fixedwing_drone"
        red_args = (0.002, 0.6, 1, 1.5, 0.6, 0.3, mode_parameters[0], mode_parameters[1], mode_parameters[2])
        sol = odeint(position, initial_point, t, args=red_args, hmax=time_step)

        trace = np.column_stack((t, sol))
        return trace
    else:
        raise ValueError("Mode: ", mode, "is not defined for the Fixedwing Drone")


def get_transform_information(mode_parameters: List[float], initset: pc.Polytope) -> Tuple[np.array, float]:
    old_center = np.average(PolyUtils.get_bounding_box(initset), axis=0)
    dot = (mode_parameters[1] - old_center[1])  # dot product between [x1, y1] and [x2, y2]
    det = (mode_parameters[0] - old_center[0])  # determinant
    dir_angle = math.atan2(det, dot)
    translation_vector: np.array = np.zeros((5,))
    translation_vector[:2] = -1 * np.array(mode_parameters[0:2])
    translation_vector[3] = -1 * dir_angle
    print(translation_vector, dir_angle)
    return translation_vector, dir_angle


def transform_poly_to_virtual(poly, transform_information):
    translation_vector, new_system_angle = transform_information
    print(translation_vector)
    print(poly)
    poly_out: pc.Polytope = poly.translation(translation_vector)
    return poly_out.rotation(i=0, j=1, theta=new_system_angle)


def transform_poly_from_virtual(poly, transform_information):
    translation_vector, new_system_angle = transform_information
    new_system_angle = -1 * new_system_angle
    translation_vector = -1 * translation_vector
    out_poly: pc.Polytope = poly.rotation(i=0, j=1, theta=new_system_angle)
    return out_poly.translation(translation_vector)


def get_virtual_mode_parameters():
    return [0.0, 0.0]


def get_flowstar_parameters():
    pass
