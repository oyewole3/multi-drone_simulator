from scipy.integrate import odeint
import numpy as np
import math
from typing import Optional, List, Tuple
import polytope as pc
from src.PolyUtils import PolyUtils


# function to return derivatives of state to be integrated
def position(p: np.array, t: float, cd1: float, vc: float, m: float,
             k1: float, k2: float, x_n: float, y_n: float) -> np.array:
    # get initial conditions
    if p.shape != (4,):
        print("bad shape:", p.shape)
        raise ValueError("p must be a length 4 array for drone")

    x: float = p[0]  # x_i
    y: float = p[1]  # y_i
    s: float = p[2]  # psi
    v: float = p[3]  # velocity
    # constants
    G: float = 32.2  # ft/sec

    # compute dvdt
    # vc = math.sqrt((x - x_n)*(x - x_n)+(y - y_n)*(y - y_n)) / ts
    D: float = cd1 * v ** 2  # drag
    T: float = k1 * m * (vc - v)  # thrust
    dvdt: float = (T - D) / m  # v'

    dot: float = (y_n - y)  # dot product between [x1, y1] and [x2, y2]
    det: float = (x_n - x)  # determinant
    sh: float = math.atan2(det, dot)  # atan2(y, x) or atan2(sin, cos)
    h: float = (k2 * vc / G) * (sh - s)
    dsdt: float = (G / v) * np.sin(h)  # * (sh - s) #

    dxdt: float = v * np.sin(s)
    dydt: float = v * np.cos(s)
    return np.array((dxdt, dydt, dsdt, dvdt))


# function to provide traces of the system
def TC_Simulate(mode: str, mode_parameters: Optional[List[float]], time_bound: float, time_step: float,
                initial_point: np.array) -> np.array:
    if mode == 'follow_waypoint':
        # mode parameters for this is the waypoint center
        t = np.arange(0, time_bound + time_step, time_step)
        assert isinstance(mode_parameters, list) and (len(mode_parameters) == 2) and (isinstance(
            mode_parameters[0], float) or isinstance(
            mode_parameters[0], int)), "must give length 2 list as params to follow_waypoint mode of fixedwing_drone"
        red_args = (0.002, 0.6, 1, 1.5, 0.6, mode_parameters[0], mode_parameters[1])
        sol = odeint(position, initial_point, t, args=red_args, hmax=time_step)
        trace = np.column_stack((t, sol))
        return trace
    else:
        raise ValueError("Mode: ", mode, "is not defined for the Fixedwing Drone")

"""
def get_transform_information(mode_parameters: List[float],  prev_mode_parameters: List[float], ) -> Tuple[np.array, float]:
    old_center = np.average(PolyUtils.get_bounding_box(initset), axis=0)
    dot = (mode_parameters[1] - old_center[1])  # dot product between [x1, y1] and [x2, y2]
    det = (mode_parameters[0] - old_center[0])  # determinant
    dir_angle = math.atan2(det, dot)
    translation_vector: np.array = np.zeros((4,))
    translation_vector[:2] = -1 * np.array(mode_parameters)
    translation_vector[2] = -1 * dir_angle
    print(translation_vector, dir_angle)
    return translation_vector, dir_angle
"""


def get_transform_information(mode_parameters: List[float], prev_mode_parameters: List[float],
                              mode: str = "follow_waypoint") -> Tuple[np.array, float]:
    if mode != "follow_waypoint":
        raise NotImplementedError("haven't implemented modes other than follow waypoint for these linear dynamics")
    old_center = prev_mode_parameters
    dot = (mode_parameters[1] - old_center[1])  # dot product between [x1, y1] and [x2, y2]
    det = (mode_parameters[0] - old_center[0])  # determinant
    dir_angle = math.atan2(det, dot)
    translation_vector: np.array = np.zeros((4,))
    translation_vector[:2] = -1 * np.array(mode_parameters)
    translation_vector[2] = 0 #  -1 * dir_angle
    return translation_vector, dir_angle


def transform_poly_to_virtual(poly, transform_information):
    translation_vector, new_system_angle = transform_information
    poly_out: pc.Polytope = poly.translation(translation_vector)
    return poly_out.rotation(i=0, j=1, theta=0)  # new_system_angle


def transform_mode_to_virtual(point, transform_information):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    translation_vector, sc = transform_information
    x_n = translation_vector[0]
    y_n = translation_vector[1]
    # x2 = (x1 + x_n) * math.cos(sc) - (y1 + y_n) * math.sin(sc)
    # y2 = (x1 + x_n) * math.sin(sc) + (y1 + y_n) * math.cos(sc)
    x2 = x1 + x_n
    y2 = y1 + y_n
    x2 = round(x2)
    y2 = round(y2)
    return [x2, y2]


def transform_poly_from_virtual(poly, transform_information):
    new_system_angle = -1 * transform_information[1]
    translation_vector = -1 * transform_information[0]
    out_poly: pc.Polytope = poly.rotation(i=0, j=1, theta=0) # new_system_angle
    return out_poly.translation(translation_vector)


def transform_mode_from_virtual(point, transform_information):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    translation_vector, sc = transform_information
    x_n = translation_vector[0]
    y_n = translation_vector[1]
    # x2 = x1 * math.cos(sc) + y1 * math.sin(sc) - x_n
    # y2 = -1 * x1 * math.sin(sc) + y1 * math.cos(sc) - y_n
    x2 = x1 - x_n
    y2 = y1 - y_n
    return [x2, y2]

def transform_state_from_then_to_virtual_dryvr_string(point, transform_information_from, transform_information_to):
    x1 = point[0]  # x_i
    y1 = point[1]  # y_i
    z1 = point[2]
    translation_vector_from, dir_angle_from = transform_information_from
    translation_vector_to, dir_angle_to = transform_information_to
    x_n_from = translation_vector_from[0]
    y_n_from = translation_vector_from[1]
    z_n_from = translation_vector_from[2]
    x2 = x1 - x_n_from
    y2 = x1 - y_n_from
    x_n_to = translation_vector_to[0]
    y_n_to = translation_vector_to[1]
    z_n_to = translation_vector_to[2]
    x3 = (x2 + x_n_to)
    y3 = (y2 + x_n_to)
    x_reset_string = "x " + " -1 * (" + str(x_n_from) + ")"
    y_reset_string = "x " + "-1 * ( " + str(y_n_from)+ ")"
    x_reset_string_2 = "x = (" + x_reset_string + " + " + str(x_n_to) + ")"
    y_reset_string_2 = "y = (" + y_reset_string + " + " + str(y_n_to) + ")"
    z_reset_string_2 = "z = z -1 * (" + str(z_n_from) + ") + " + str(z_n_to)
    return x_reset_string_2 + ";" + y_reset_string_2 + "; " + z_reset_string_2

def get_virtual_mode_parameters():
    return [0.0, 0.0]

def get_flowstar_parameters():
    pass
