from scipy.integrate import odeint
import numpy as np
import math as m
from typing import Optional, List, Tuple
from src.Waypoint import Waypoint
import polytope as pc
from src.PolyUtils import PolyUtils

A = [[-3.0, 1.0, 0.0], [0.0, -2.0, 1.0], [0.0, 0.0, -1.0]]
# Dynamics of the linear system
def linear_dynamic(x, t, x_d):
    dvdt = np.dot(np.array(A), x) - np.dot(np.array(A), np.array(x_d))
    return dvdt

def simulation_single(system_dynamics, initial_state, time_step, time_bound, x_d):
    time_bound = float(time_bound)
    time_step = float(time_step)
    number_points = int(np.ceil(time_bound/time_step))
    t = [i*time_step for i in range(0,number_points)]
    if t[-1] != time_step:
        t.append(time_bound)
    newt = []
    for step in t:
        newt.append(float(format(step, '.4f')))
    t = np.array(newt)

    sol = odeint(system_dynamics, initial_state, t, args=(x_d,), hmax=time_step)

    # Construct the final output
    trace = np.column_stack((t, sol[:, :3]))
    return trace

def TC_Simulate(mode: str, mode_parameters: Optional[List[float]], time_bound: float, time_step: float,
                initial_point: np.array) -> np.array:
    if mode == 'follow_waypoint':
        assert isinstance(mode_parameters, list) and (len(mode_parameters) == 3) and isinstance(
            mode_parameters[0], float), "must give length 3 list as params to follow_waypoint mode of 3D follower"
        trace = simulation_single(linear_dynamic, initial_point, time_step, time_bound, mode_parameters)
        return trace
    else:
        raise ValueError("Mode: ", mode, "is not defined for the 3D linear follower")

def get_transform_information(mode_parameters: List[float], initset: pc.Polytope, mode: str ="follow_waypoint") -> Tuple[np.array, float]:
    if mode != "follow_waypoint":
        raise NotImplementedError("haven't implemented modes other than follow waypoint for these linear dynamics")
    old_center = np.average(PolyUtils.get_bounding_box(initset), axis=0)
    dot = (mode_parameters[1] - old_center[1])  # dot product between [x1, y1] and [x2, y2]
    det = (mode_parameters[0] - old_center[0])  # determinant
    dir_angle = m.atan2(det, dot)
    det2 = (mode_parameters[2] - old_center[2])  # determinant
    # dir_angle.append(m.atan2(det2, dot))
    dir_angle2 = m.atan2(dot, det2)
    translation_vector = -1 * np.array(mode_parameters)
    return translation_vector, dir_angle, dir_angle2

def transform_poly_to_virtual(poly, transform_information):
    translation_vector, new_system_angle, new_system_angle2 = transform_information
    poly_out: pc.Polytope = poly.translation(translation_vector)
    poly_out = poly_out.rotation(i=0, j=1, theta=new_system_angle)
    return poly_out.rotation(i=1, j=2, theta=new_system_angle2)

def transform_poly_from_virtual(poly, transform_information):
    new_system_angle2 = -1 * transform_information[2]
    new_system_angle = -1 * transform_information[1]
    translation_vector = -1 * transform_information[0]
    out_poly: pc.Polytope = poly.rotation(i=1, j=2, theta=new_system_angle2)
    out_poly = out_poly.rotation(i=0, j=1, theta=new_system_angle)
    return out_poly.translation(translation_vector)

def get_virtual_mode_parameters():
    return [0.0, 0.0, 0.0]

def get_flowstar_parameters(mode_parameters: List[float], initial_set: np.array, time_step: float, time_bound: float, mode: str):
    if mode != "follow_waypoint":
        raise NotImplementedError("These linear dynamics only support waypoint following mode")
    num_vars = len(A) + 1
    order = 4
    hyper_params = [str(num_vars), str(time_step), str(time_bound), str(order)]
    cur_list: List[str] = hyper_params[:]
    var_names: List[str] = ["t", "x", "y", "z"]
    cur_list.extend(var_names[:num_vars])
    ode_rhs = ["1"]
    ode_rhs.extend([' + '.join([str(val) + " * (" + var_names[ind+1] + ' - ' + "(" + str(mode_parameters[ind]) + "))"
                                 for ind, val in enumerate(A_row)]) for A_row in A])
    cur_list.extend(ode_rhs)
    # time initset lowerbound
    cur_list.append('0')
    cur_list.extend([str(val) for val in initial_set[0, :]])
    # time initset upperbound
    cur_list.append('0')
    cur_list.extend([str(val) for val in initial_set[1, :]])
    return cur_list

if __name__ == '__main__':
    print('"'+'" "'.join(get_flowstar_parameters([5, 5, 5], np.array([[0, 0, 0], [1, 1, 1]]), 0.05, 5, "follow_waypoint"))+ '"')