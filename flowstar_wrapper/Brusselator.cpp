#include "./flowstar/Continuous.h"
// #include "../flowstar/modelParser.h"

using namespace flowstar;
using namespace std;

#define MIN_ARG_COUNT (2)
#define NUM_HYPERPARAMS (4)
#define NUM_VAR_IND (1)
#define TIME_STEP_IND (2)
#define TIME_HORIZON_IND (3)
#define ORDER_IND (4)
#define GET_VARNAME_IND(i) ((i)+NUM_HYPERPARAMS+1)
#define GET_DYNAMICS_IND(i, n) ((i)+NUM_HYPERPARAMS+1+(n))
#define LB_INT_IND(i, n) ((i)+NUM_HYPERPARAMS+1+2*(n))
#define UB_INT_IND(i, n) ((i)+NUM_HYPERPARAMS+1+3*(n))
template<typename T>
void destroy_vector(std::vector<T*> &v)
{
    while(!v.empty()) {
        delete v.back();
        v.pop_back();
    }
}

int main(int argc, char* argv[])
{
    if (argc < MIN_ARG_COUNT){
        cout << "ERROR bad arguments";
    }
    vector<string *> my_arg_pointers(argc);
    vector<string> my_args(argc);
    for(int i = 0; i < argc; i++){
        my_arg_pointers[i] = new string(argv[i]);
        my_args[i] = *(my_arg_pointers[i]);
    }
    unsigned int numVars = stoi(my_args[NUM_VAR_IND]);
    vector<string> var_name(numVars);
    vector<int> var_id(numVars);
    // VarList stateVars();
    for(int i = 0; i < numVars; i++){
        var_name[i] = my_args[GET_VARNAME_IND(i)];
        var_id[i] = stateVars.declareVar(var_name[i]);
    }
    // define the dynamics
    vector<Expression_AST<Real> * > ode_remember_pointers(numVars);
    vector<Expression_AST<Real>> ode_rhs(numVars);
    for(int i = 0; i < numVars; i++){
        ode_remember_pointers[i] = new Expression_AST<Real>(my_args[GET_DYNAMICS_IND(i,numVars)]);
        ode_rhs[i] = *(ode_remember_pointers[i]);
    }

    Deterministic_Continuous_Dynamics dynamics(ode_rhs);

    // set the reachability parameters
    Computational_Setting setting;

    float timeStep = stof(my_args[TIME_STEP_IND]);

    unsigned int order = stoi(my_args[ORDER_IND]);

    // set the stepsize and the order
    setting.setFixedStepsize(timeStep, order);
//	setting.setFixedStepsize(0.05, 5, 8);			// adaptive orders
//	setting.setAdaptiveStepsize(0.01, 0.05, 5);		// adaptive stepsizes

    // set the time horizon
    unsigned int timeHorizon = stoi(my_args[TIME_HORIZON_IND]);
    setting.setTime(timeHorizon);

    // set the cutoff threshold
    setting.setCutoffThreshold(1e-7);

    // set the queue size for the symbolic remainder, it is 0 if symbolic remainder is not used
    setting.setQueueSize(100);

    // print out the computation steps
    // setting.printOn();

    // set up the remainder estimation
    Interval I(-0.01, 0.01);
    vector<Interval> remainder_estimation(numVars, I);
    setting.setRemainderEstimation(remainder_estimation);

    // call this function when all of the parameters are defined
    setting.prepare();


    // define the initial set which is a box
    vector<Interval *> initial_box_pointers(numVars);
    vector<Interval> initial_box(numVars);
    for(int i = 0; i < numVars; i++){
        initial_box_pointers[i] = new Interval(stoi(my_args[LB_INT_IND(i, numVars)]), stoi(my_args[UB_INT_IND(i, numVars)]));
        initial_box[i] = *(initial_box_pointers[i]);
    }

    Flowpipe initialSet(initial_box);


    // empty unsafe set
    vector<Constraint> unsafeSet;


    /*
     * The structure of the class Result_of_Reachability is defined as below:
     * nonlinear_flowpipes: the list of computed flowpipes
     * tmv_flowpipes: translation of the flowpipes, they will be used for further analysis
     * fp_end_of_time: the flowpipe at the time T
     */
    Result_of_Reachability result;

    // run the reachability computation
    clock_t begin, end;
    begin = clock();

    dynamics.reach(result, setting, initialSet, unsafeSet);

    end = clock();
    printf("time cost: %lf\n", (double)(end - begin) / CLOCKS_PER_SEC);
    // flowpipes should be translated to single Taylor model vectors before plotting
    result.transformToTaylorModels(setting);

    list<vector<Interval> > boxes;
    result.computeBoxOverapproximations(boxes, setting);

    list<vector<Interval> >::iterator iter = boxes.begin();
    for(; iter != boxes.end(); ++iter)
    {
        for(int i=0; i<iter->size(); ++i)
        {
            (*iter)[i].dump(stdout);
            printf(" ");
        }

        printf("\n");
    }

    /*
        Plot_Setting plot_setting;
        plot_setting.printOn();
        plot_setting.setOutputDims(x_id, y_id);
        plot_setting.discreteOutput();

        plot_setting.plot_2D_interval_MATLAB("Brusselator", result);
    */

    // destroy_vector(ode_remember_pointers);
    // destroy_vector(inital_box_pointers);
    return 0;
}
