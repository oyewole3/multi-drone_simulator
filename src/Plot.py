import numpy as np
import polytope as pc
from src.PolyUtils import PolyUtils
from src.ReachtubeSegment import ReachtubeSegment
from src.Agent import Agent, DRONE_TYPE
from typing import List, Dict, Tuple
from scipy.spatial import ConvexHull
import matplotlib
matplotlib.use("macOSX")
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Polygon
from src.Agent import UniformTubeset

class Plotter:
    # colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'b', 'g']
    # colors = ['#8dd3c7', '#ffffb3', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#b3de69', '#fccde5']
    # colors = ['#1b9e77', '#d95f02', '#7570b3', '#e7298a', '#66a61e', '#e6ab02', '#a6761d', '#666666']
    colors = ['#8dd3c7',  '#bebada', '#b3de69', '#80b1d3', '#fdb462', '#fccde5', '#d9d9d9', '#bc80bd', '#ccebc5', '#ffed6f', '#ffffb3', '#c7eae5']
    # colors = ['#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7', '#8dd3c7']
    # TODO move away from static plotting and move towards OO plotting configurations
    @staticmethod
    def plot(drone_num, tubes: List[List[ReachtubeSegment]], agents_list: List[Agent], is_unified: bool, unsafe_set, virtual_mode_to_id, plot_step=1):
        # agents_tubes = [[segment.tube for segment in segment_list] for segment_list in tubes]
        plt.figure()
        currentAxis = plt.gca()
        waypointslists = [[waypoint.mode_parameters for waypoint in agent.path] for agent in agents_list]
        legend_drone_rect = []
        legend_unsafe_set = []
        for d in range(drone_num):
            curr_agent = agents_list[d]
            if not is_unified:
                wp = np.array(waypointslists[d])
                if d == 0:
                    plt.plot(wp[:, 0], wp[:, 1], 'k' + 'o', label='waypoints', linewidth= 1, markersize=1)
                    plt.plot(wp[:, 0], wp[:, 1], 'k', linewidth=1, markersize=1)
                else:
                    plt.plot(wp[:, 0], wp[:, 1], 'k' + 'o', linewidth=1, markersize=1)
                    plt.plot(wp[:, 0], wp[:, 1], 'k', linewidth=1, markersize=1)

            curr_tubes = tubes[d]
            for ci in range(len(curr_tubes)):
                curr_segment = curr_tubes[ci]
                if virtual_mode_to_id != None:
                    color = Plotter.colors[virtual_mode_to_id[curr_segment.virtual_mode]]
                else:
                    color = Plotter.colors[0]
                curr_tube = curr_segment.tube
                traces = curr_segment.trace
                for i in range(0, len(curr_tube), plot_step):
                    box_of_poly = PolyUtils.get_bounding_box(curr_tube[i])
                    rect = Rectangle(box_of_poly[0, [0, 1]], box_of_poly[1, 0] - box_of_poly[0, 0],
                                     box_of_poly[1, 1] - box_of_poly[0, 1], linewidth=1,
                                     edgecolor=color, facecolor=color)
                    if ci == 0 and i == 0:
                        legend_drone_rect.append(rect)
                    currentAxis.add_patch(rect)
        color = 'r'

        if isinstance(unsafe_set, np.ndarray):
            unsafe_set = pc.box2poly(unsafe_set.T)
        if isinstance(unsafe_set, pc.Polytope):
            unsafe_set = [unsafe_set]
        for i in range(len(unsafe_set)):
            poly = unsafe_set[i]
            points = pc.extreme(poly)
            points = points[:, :2]
            hull = ConvexHull(points)
            poly_patch = Polygon(points[hull.vertices, :], alpha=.5, color=color, fill=True)
            if i == 0:
                legend_unsafe_set.append(poly_patch)
            currentAxis.add_patch(poly_patch)

        # plt.legend((legend_drone_rect[0], legend_drone_rect[1], legend_unsafe_set[0]), ("first drone tube", "second drone tube", "unsafe set"))
        # plt.ylim([-40, 40])
        # plt.xlim([-55, 55])
        plt.ylim([-8, 26])
        plt.xlim([-20, 75])
        #plt.ylim([-30, 30])
        #plt.xlim([-55, 40])
        #plt.ylim([-8, 26])
        #plt.xlim([-20, 75])
        #plt.ylim([-10, 30])
        #plt.xlim([-10, 60])
        plt.tick_params(axis='both', which='major', labelsize=15)
        plt.xlabel('x', fontsize=15)
        plt.ylabel('y', fontsize=15)
        #plt.tight_layout()
        # plt
        plt.show()

    # def plot_virtual(tubes: List[List[ReachtubeSegment]], agents_list: List[Agent], is_unified: bool, unsafe_set,
    #         plot_step=1):
    @staticmethod
    def plot_virtual(tube_dict: Dict[Tuple[float, ...], List[np.array]], virtual_mode_to_id, abs_nodes_neighbors,
                     plot_step=1):
        # agents_tubes = [[segment.tube for segment in segment_list] for segment_list in tubes]
        plt.figure()
        currentAxis = plt.gca()
        # waypointslists = [[waypoint.mode_parameters for waypoint in agent.path] for agent in agents_list]
        legend_drone_rect = []
        legend_unsafe_set = []
        color_indx = 0
        for virtual_mode in tube_dict:
            color = Plotter.colors[virtual_mode_to_id[virtual_mode]]
            curr_tube = tube_dict[virtual_mode]
            # print("tube of mode ", virtual_mode, " is:", curr_tube)
            for next_node in abs_nodes_neighbors[virtual_mode]:
                print(virtual_mode, "->", next_node)
                plt.plot([virtual_mode[0], next_node[0]], [virtual_mode[1], next_node[1]], 'k' + 'o', label='waypoints', linewidth=1, markersize=1)
                plt.plot([virtual_mode[0], next_node[0]], [virtual_mode[1], next_node[1]], 'k', linewidth=1, markersize=1)
            """
            wp = np.array(waypointslists[d])
            if d == 0:
                plt.plot(wp[:, 0], wp[:, 1], 'k' + 'o', label='waypoints')
                plt.plot(wp[:, 0], wp[:, 1], 'k')
            else:
                plt.plot(wp[:, 0], wp[:, 1], 'k' + 'o')
                plt.plot(wp[:, 0], wp[:, 1], 'k')
            """
            for i in range(0, len(curr_tube), plot_step):
                box_of_poly = curr_tube[i]  # PolyUtils.get_region_bounding_box(curr_tube[i])
                rect = Rectangle(box_of_poly[0, [0, 1]], box_of_poly[1, 0] - box_of_poly[0, 0],
                                 box_of_poly[1, 1] - box_of_poly[0, 1], linewidth=1,
                                 edgecolor=color, facecolor=color)
                if i == 0:
                    legend_drone_rect.append(rect)
                currentAxis.add_patch(rect)
            color_indx = color_indx + 1
        """
        color = 'r'
        if isinstance(unsafe_set, np.ndarray):
            unsafe_set = pc.box2poly(unsafe_set.T)
        if isinstance(unsafe_set, pc.Polytope):
            unsafe_set = [unsafe_set]
        for i in range(len(unsafe_set)):
            poly = unsafe_set[i]
            points = pc.extreme(poly)
            points = points[:, :2]
            hull = ConvexHull(points)
            poly_patch = Polygon(points[hull.vertices, :], alpha=.5, color=color, fill=True)
            if i == 0:
                legend_unsafe_set.append(poly_patch)
            currentAxis.add_patch(poly_patch)

        """
        # plt.legend((legend_drone_rect[0], legend_drone_rect[1], legend_unsafe_set[0]), ("first drone tube", "second drone tube", "unsafe set"))
        # plt.ylim([-30, 30])
        # plt.xlim([-30, 30])
        plt.tick_params(axis='both', which='major', labelsize=15)
        plt.xlabel('x', fontsize=15)
        plt.ylabel('y', fontsize=15)
        plt.tight_layout()
        # plt
        plt.show()