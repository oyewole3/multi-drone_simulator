import numpy as np
import polytope as pc
from typing import List


class Waypoint:

    original_guard: pc.Polytope

    def __init__(self, mode: str, mode_parameters: List[float], guard: List[List[float]], time_bound: float):
        self.mode: str = mode
        self.mode_parameters: List[float] = mode_parameters
        self.original_guard: np.array = np.array(guard)
        self.time_bound: float = time_bound
        self.delta: np.array = (self.original_guard[1, :] - self.original_guard[0, :]) / 2
    # TODO add helper function to check if point is inside guard
