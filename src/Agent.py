import polytope as pc
from src.Waypoint import Waypoint
import numpy as np
from scipy.integrate import odeint
import math
from src.ReachtubeSegment import ReachtubeSegment
from src.PolyUtils import PolyUtils
from typing import List, Set, Tuple, Dict, Optional
import matplotlib

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Polygon
from src.DiscrepancyLearning import DiscrepancyLearning
from src.ParseModel import importFunctions
import copy
import subprocess
import pdb
import ast
import time

DRONE_TYPE = 1


class Agent:

    def __init__(self, path: List[Waypoint], initial_set: pc.Polytope, dynamics: str, function_arr) -> None:
        self.path: List[Waypoint] = path
        self.initial_set: pc.Polytope = initial_set
        # self.local_unsafe_sets: pc.Region = local_unsafe_sets
        self.dynamics: str = dynamics
        self.transform_time = 0
        self.dim = initial_set.dim
        self.TC_Simulate = function_arr[0]
        self.get_transform_information = function_arr[1]
        self.transform_poly_to_virtual = function_arr[2]
        self.transform_mode_to_virtual = function_arr[3]
        self.transform_poly_from_virtual = function_arr[4]
        self.transform_mode_from_virtual = function_arr[5]
        self.get_virtual_mode_parameters = function_arr[6]
        self.get_flowstar_parameters = function_arr[7]

    @staticmethod
    def time_list(time_step: float, time_bound: float) -> np.array:
        return np.arange(0, time_bound + time_step, time_step)

    @staticmethod
    def reached_way_point(s: np.array, waypoint: Waypoint):
        guard: np.array = waypoint.original_guard
        # if s[0] + start_time > waypoint.time_bound:
        #    return False
        return np.all(guard[0, :] <= s[1:]) and np.all(guard[1, :] >= s[1:])

    # Function that returns the trace for an agent's initial set the time bound in goal_waypoint is in global time,
    # so we need to know what is the time bound from the previous waypoint hence, time_bound =
    # goal_waypoint.time_bound - previous goal_waypoint.time_bound
    def simulate_segment(self, initial_point: np.array, goal_waypoint: Waypoint, time_step: int, time_bound: int):
        assert type(self.dynamics) == str, "must provide path"
        trace = self.TC_Simulate(goal_waypoint.mode, goal_waypoint.mode_parameters, goal_waypoint.time_bound, time_step,
                                 initial_point)
        return trace


class TubeMaster:
    # TODO support multiple TubeCaches
    # TODO support multiple grids for each TubeCache
    # grid_resolution is the quantization resolution
    # def __init__(self, grid_resolution: np.array, dynamics_types: Dict[str, str],
    #             trans_guards_bbox_list: List[np.array], trans_info_list) -> None:
    def __init__(self, grid_resolution: np.array, dynamics_types: Dict[str, str],
                 abs_edges_guards, abs_edges_guards_union, abs_reach, abs_initset, abs_nodes_neighbors,
                 sym_level) -> None:

        # self.trans_guards_bbox_list = trans_guards_bbox_list
        # self.trans_info_list = trans_info_list
        self.abs_edges_guards = abs_edges_guards
        self.abs_edges_guards_union = abs_edges_guards_union
        self.abs_reach = abs_reach
        self.abs_initset = abs_initset
        self.abs_nodes_neighbors = abs_nodes_neighbors
        # self.tube_tools: Dict[str, Tuple[TubeComputer, TubeCache]] = {
        #    dynamics: TubeCache(TubeComputer(dynamics_types[dynamics]), self.trans_guards_bbox_list,
        #                        self.trans_info_list) for
        #    dynamics in dynamics_types}
        self.tube_tools: Dict[str, Tuple[TubeComputer, TubeCache]] = {
            dynamics: TubeCache(TubeComputer(dynamics_types[dynamics]), self.abs_edges_guards,
                                self.abs_edges_guards_union,
                                self.abs_reach, self.abs_initset, self.abs_nodes_neighbors, sym_level) for
            dynamics in dynamics_types}
        self.grid_resolution: np.array = grid_resolution
        self.reached_fixed_point = False
        self.sym_level = sym_level
        # TODO support other kinds of discrepancy functions, etc.

    @staticmethod
    def quantize_uniform(center: np.array, delta: np.array) -> np.array:
        if len(center.shape) == 1 and len(delta.shape) == 1:
            raise TypeError("Quantize Uniform only accepts 1d numpy arrays")
        return np.floor(center / delta, dtype=np.int)

    # TODO: Implement Unified Spaces and Tube class
    def get_tube(self, cur_agent: Agent, initset: pc.Polytope, waypoint: Waypoint, prev_waypoint: Waypoint,
                 next_waypoint: Waypoint, is_last_waypoint: bool,
                 time_step: float) -> Tuple[List[pc.Polytope], List[np.array], int, int, pc.Polytope]:
        curr_tube_cache: TubeCache
        curr_tube_cache = self.tube_tools[cur_agent.dynamics]

        if self.sym_level != 2 or not self.reached_fixed_point:
            tube, trace, min_index, max_index, next_initset_u, transform_information, transform_time, fixed_point = \
                curr_tube_cache.get(
                    cur_agent, initset, waypoint, prev_waypoint, next_waypoint, is_last_waypoint, time_step,
                    self.grid_resolution)
            if self.sym_level == 2:
                self.reached_fixed_point = fixed_point
            if self.sym_level != 0:
                next_initset_temp = cur_agent.transform_poly_from_virtual(pc.box2poly(next_initset_u.T),
                                                                          transform_information)
                next_initset = next_initset_temp  # pc.intersect(next_initset_temp, pc.box2poly(waypoint.original_guard.T))
            else:
                next_initset = pc.box2poly(next_initset_u.T)
        else:
            transform_information = cur_agent.get_transform_information(waypoint.mode_parameters,
                                                                        prev_waypoint.mode_parameters)
            abs_prev_waypoint = tuple(cur_agent.transform_mode_to_virtual(prev_waypoint.mode_parameters,
                                                                          transform_information))
            tube = curr_tube_cache.abs_reach[abs_prev_waypoint]
            curr_tube_cache.ultrasaved_counter += curr_tube_cache.abs_per_virtual_mode_tube_counter[abs_prev_waypoint]
            min_index = 0  # TODO
            max_index = len(tube) - 1  # TODO
            trace = []
            next_initset = pc.Region(list_poly=[])
            transform_time = 0
        t = time.time()
        if self.sym_level != 0:
            orig_tube = [cur_agent.transform_poly_from_virtual(pc.box2poly(tube[i][:, :].T), transform_information) for
                         i in
                         range(len(tube))]
            transform_time += time.time() - t
        else:
            orig_tube = [pc.box2poly(tube[i][:, :].T) for i in range(len(tube))]
            transform_time = 0
        if not self.reached_fixed_point:
            assert not pc.is_empty(
                next_initset), "next initset should not be empty, system does not reach the next mode's guard"
        return orig_tube, trace, min_index, max_index, next_initset, transform_time, self.reached_fixed_point

    @staticmethod
    def plot_segment(curr_tube: List[np.array], is_unified: bool):
        plt.figure()
        curr_tube = [tube[:, 1:] for tube in curr_tube]
        currentAxis = plt.gca()
        bigger_box = None
        if is_unified:
            for i in range(len(curr_tube)):
                box_of_poly = curr_tube[i]
                bigger_box = box_of_poly if bigger_box is None else np.row_stack(
                    (np.minimum(box_of_poly[0, :], bigger_box[0, :]),
                     np.maximum(box_of_poly[1, :], bigger_box[1, :])))
                rect = Rectangle(box_of_poly[0, [0, 1]], box_of_poly[1, 0] - box_of_poly[0, 0],
                                 box_of_poly[1, 1] - box_of_poly[0, 1], linewidth=1, facecolor='red')
                currentAxis.add_patch(rect)
            plt.xlim(bigger_box[0, 0] * 1.1, max(bigger_box[1, 0] * 1.1, 0))
            plt.ylim(bigger_box[0, 1] * 1.1, max(bigger_box[1, 1] * 1.1, 0))

        plt.ylim([-2, 14])
        # plt.xlim([-5, 25])
        plt.xlabel('x')
        plt.ylabel('y')
        plt.show()

    @staticmethod
    def plot_segments(curr_tubes: List[List[pc.Region]], is_unified: bool):
        plt.figure()
        currentAxis = plt.gca()
        bigger_box = None
        if is_unified:
            for curr_tube in curr_tubes:
                for i in range(len(curr_tube)):
                    box_of_poly = PolyUtils.get_region_bounding_box(curr_tube[i])
                    bigger_box = box_of_poly if bigger_box is None else np.row_stack(
                        (np.minimum(box_of_poly[0, :], bigger_box[0, :]),
                         np.maximum(box_of_poly[1, :], bigger_box[1, :])))
                    rect = Rectangle(box_of_poly[0, [0, 2]], box_of_poly[1, 0] - box_of_poly[0, 0],
                                     box_of_poly[1, 2] - box_of_poly[0, 2], linewidth=1, facecolor='red')
                    currentAxis.add_patch(rect)
            plt.xlim(bigger_box[0, 0] * 1.1, max(bigger_box[1, 0] * 1.1, 0))
            plt.ylim(bigger_box[0, 1] * 1.1, max(bigger_box[1, 1] * 1.1, 0))
        else:
            raise NotImplementedError("No support for original tubes in plot segments")
        plt.xlabel('x')
        plt.ylabel('y')
        plt.show()

    @staticmethod
    def intersect_waypoint(tube: List[np.array], guard: np.array) -> Tuple[int, int, np.array]:
        min_index: int = -1
        max_index: int = -1
        i: int
        is_empty = True
        inter_list = []
        guard_contains_tube = False
        tube_contains_guard = False

        for i in range(len(tube)):
            if PolyUtils.do_rects_inter(tube[i], guard):
                inter_list.append(PolyUtils.get_rects_inter(tube[i], guard))
                is_empty = False
                if min_index == -1:
                    min_index = i
                if i > max_index:
                    max_index = i
        if is_empty:
            TubeMaster.plot_segment(tube, True)
            pdb.set_trace()
        else:
            pass
            # print("found next initset")

        next_initset: np.array = PolyUtils.get_convex_union(inter_list)
        if guard_contains_tube:
            next_initset = tube[i][:, :]
            max_index = i
            if min_index == -1:
                min_index = i
        elif tube_contains_guard:
            next_initset = np.copy(guard)
            max_index = i
            if min_index == -1:
                min_index = i
        return min_index, max_index, next_initset

    """
    @staticmethod
    def intersect_waypoint_list(tube: List[np.array], guard: List[np.array]) -> np.array:
        i: int
        inter_list = []
        is_empty = True
        for i in range(len(tube)):
            for j in range(len(guard)):
                if PolyUtils.do_rects_inter(tube[i], guard[j]):
                    inter_list.append(PolyUtils.get_rects_inter(tube[i], guard[j]))
                    is_empty = False
        if is_empty:
            TubeMaster.plot_segment(tube, True)
            pdb.set_trace()
        else:
            pass
        return PolyUtils.get_convex_union(inter_list)
    """

    @staticmethod
    def intersect_waypoint_list(tube: List[np.array],
                                guard: List[Tuple[np.array, Tuple[float, ...], Tuple[float, ...]]]) -> np.array:
        i: int
        is_empty = True
        result_list = []
        min_index: int = -1
        max_index: int = -1
        guard_contains_tube = False
        tube_contains_guard = False
        for j in range(len(guard)):
            inter_list = []
            for i in range(len(tube)):
                if PolyUtils.do_rects_inter(tube[i], guard[j][0]):
                    inter_list.append(PolyUtils.get_rects_inter(tube[i], guard[j][0]))
                    is_empty = False
                    if min_index == -1:
                        min_index = i
                    if i > max_index:
                        max_index = i
            result_list.append((PolyUtils.get_convex_union(inter_list), guard[j][1], guard[j][2]))

        if is_empty:
            TubeMaster.plot_segment(tube, True)
            pdb.set_trace()
        else:
            pass
        return min_index, max_index, result_list

    def get_tubesegment(self, cur_agent: Agent, initset: pc.Polytope, waypoint: Waypoint, prev_waypoint: Waypoint,
                        next_waypoint: Waypoint, is_last_waypoint: bool,
                        time_step: float) -> ReachtubeSegment:
        # it would use the unsafeset for refinement
        tube: np.array
        trace: np.array
        min_index: int
        max_index: int
        next_initset: pc.array
        # TODO convert this tube to polytopes probably earlier in code
        tube, traces, min_index, max_index, next_initset, transform_time, fixed_point = self.get_tube(cur_agent,
                                                                                                      initset, waypoint,
                                                                                                      prev_waypoint,
                                                                                                      next_waypoint,
                                                                                                      is_last_waypoint,
                                                                                                      time_step)
        # TODO optimize this with PolyUtils function to skip box2poly
        # change for TAC, uncomment later
        # tube = tube[:max_index]
        if not fixed_point:
            if traces[0] == None:
                trace = None
            else:
                trace = traces[0][:max_index, :]
        else:
            trace = []
        is_unified: bool = False
        system_origin: np.array = np.zeros((2,))
        system_angle: float = 0.0
        if self.sym_level != 0:
            transform_information = cur_agent.get_transform_information(waypoint.mode_parameters,
                                                                        prev_waypoint.mode_parameters)
            abs_prev_waypoint = tuple(cur_agent.transform_mode_to_virtual(prev_waypoint.mode_parameters,
                                                                          transform_information))
        else:
            abs_prev_waypoint = None
        output: ReachtubeSegment = ReachtubeSegment(tube, trace, system_origin, system_angle, is_unified, min_index,
                                                    max_index, next_initset, abs_prev_waypoint)
        return output, transform_time, fixed_point

    @staticmethod
    def trim_trace(trace: np.array, waypoint: Waypoint) -> Optional[np.array]:
        last_reached: int = -1
        i: int
        for i in range(len(trace)):
            if Agent.reached_way_point(trace[i], waypoint):
                last_reached: int = i
        new_trace: Optional[np.array] = None
        if last_reached > -1:
            new_trace: Optional[np.array] = trace[:last_reached, :]
        return new_trace


# This class would compute tubes as lists of hyper-rectangles
class TubeComputer:
    def __init__(self, reachability_engine: str):
        self.reachability_engine: str = reachability_engine
        pass

    def compute_tube(self, cur_agent: Agent, initset: np.array, waypoint: Waypoint,
                     time_step: int) -> Tuple[np.array, List[np.array]]:
        # compute trace of the agent from the center
        # bloat the simulation to tube
        # refine based on the unsafe set
        # get intersection with the waypoint
        if self.reachability_engine == "default":
            k: np.array
            gamma: np.array
            k, gamma, center_trace = DiscrepancyLearning.compute_k_and_gamma(initset, waypoint, time_step,
                                                                             waypoint.time_bound, cur_agent)
            discrepancy = np.row_stack((k, gamma))  # replace it with the discrepancy computer
            # init_delta_array = (initset_box[1, :] - initset_box[0, :]) / 2
            # print("computetube_initset: ", initset)
            # print("center trace: ", center_trace[0])
            init_delta_array = (initset[1, :] - initset[0, :]) / 2
            tube = TubeComputer.bloat_to_tube(discrepancy, init_delta_array, center_trace)
            return center_trace, tube
        elif self.reachability_engine == "flowstar":
            flow_params = cur_agent.get_flowstar_parameters(waypoint.mode_parameters, initset, time_step,
                                                            waypoint.time_bound, waypoint.mode)
            tube = TubeComputer.get_flowstar_tube(flow_params)
            return None, tube
        raise NotImplementedError("System does not support other reachability types")

    # duration here is the maximum time at which the tube intersects the guard minus the minimum time it reached it.

    @staticmethod
    def bloat_to_tube(discrepancy, init_delta_array, trace):
        '''
            guard: is a polytope that represents the target of the tube
        '''
        if trace.shape[0] < 1:
            pdb.set_trace()
            raise ValueError("Trace is too small")
        k = discrepancy[0, :]
        gamma = discrepancy[1, :]
        time_intervals = trace[1:, 0] - trace[0, 0]
        deltas = np.exp(np.outer(time_intervals, gamma)) * np.tile(init_delta_array * k, (time_intervals.shape[0], 1))
        reach_tube = np.stack((np.minimum(trace[1:, :], trace[:-1, :]) - deltas,
                               np.maximum(trace[1:, :], trace[:-1, :]) + deltas), axis=1)
        reach_tube = list(reach_tube)
        # print("computed tube initial set: ", reach_tube[0])
        return reach_tube

    @staticmethod
    def get_flowstar_tube(params):
        ABS_TOL = 1e-4
        result = str(subprocess.check_output(["../flowstar_wrapper/Brusselator"] + params)).split('\\n')
        time: float = float(result[0][result[0].index(": ") + 1:])
        print(time)
        iotube = [val.replace("] [", "],! [").split(",! ") for val in result[1:-1]]
        my_boxes = []
        for poly in iotube:
            cur_list = []
            for arr in poly:
                cur_list.append(ast.literal_eval(arr))
            my_boxes.append(np.column_stack(cur_list))
        tube = np.stack(my_boxes)
        # Added to bloat convering flowtubes to be above our abs_tolerance for non-empty intersection
        tube[:, 0, :] -= ABS_TOL
        tube[:, 1, :] += ABS_TOL
        return tube[:, :, 1:]

    # TODO implement get reachtube functions
    # TODO find better Data-structure for queries


class UniformTubeset:

    def __init__(self, input_tube=[], input_trace=None):
        # self.tube: List[pc.Polytope] = input_tube
        self.tube: List[np.array] = input_tube
        self.trace: Optional[np.array] = input_trace


class TubeCache:

    # def __init__(self, curr_tube_computer: TubeComputer, trans_guards_bbox_list, trans_info_list):
    def __init__(self, curr_tube_computer: TubeComputer, abs_edges_guards, abs_edges_guards_union, abs_reach,
                 abs_initset, abs_nodes_neighbors,
                 sym_level):
        self.tube_computer = curr_tube_computer
        self.tube_dict: Dict[Tuple[int, ...], UniformTubeset] = dict()
        # self.tube_union: pc.Region = pc.Region(list_poly=[])
        self.initset_union: pc.Region = pc.Region(list_poly=[])
        self.initset_union_rects: List = []
        self.computed_counter = 0
        self.saved_counter = 0
        self.ultrasaved_counter = 0
        self.abs_per_virtual_mode_tube_counter: Dict[Tuple[List[float]], int] = {}
        for virtual_mode in abs_reach:
            self.abs_per_virtual_mode_tube_counter[virtual_mode] = 0
        self.uncovered_sets = pc.Region(list_poly=[])
        self.uncovered_sets_per_mode = dict()
        self.uncovered_rects_per_mode = dict()
        # self.trans_guards_bbox_list = trans_guards_bbox_list
        # self.trans_info_list = trans_info_list
        self.abs_edges_guards = abs_edges_guards
        self.abs_edges_guards_union = abs_edges_guards_union
        self.abs_reach = abs_reach
        self.abs_initset = abs_initset
        self.abs_initset_rects = {}
        for virtual_mode in self.abs_initset:
            self.abs_initset_rects[virtual_mode] = []
        self.abs_nodes_neighbors = abs_nodes_neighbors
        self.sym_level = sym_level

    def get(self, cur_agent: Agent, initset: pc.Polytope, waypoint: Waypoint, prev_waypoint: Waypoint,
            next_waypoint: Waypoint, is_last_waypoint: bool,
            time_step: float, grid_resolution) -> Tuple[List[np.array], List[np.array]]:

        transform_time = 0
        time_bound = waypoint.time_bound

        def next_quantized_key(curr_key: np.array, quantized_key_range: np.array) -> np.array:
            if len(curr_key.shape) > 1:
                raise ValueError("key must be one dimensional lower left and corner of bounding box")
            next_key = np.copy(curr_key)
            for dim in range(curr_key.shape[0] - 1, -1, -1):
                if curr_key[dim] < quantized_key_range[1, dim]:
                    next_key[dim] += 1
                    for reset_dim in range(dim + 1, curr_key.shape[0]):
                        next_key[reset_dim] = quantized_key_range[0, reset_dim]
                    return next_key
            raise ValueError("curr_key should not exceed the bounds of the bounding box.")

        # TODO all the transforming
        # print("segment initial set: ", PolyUtils.get_bounding_box(initset_poly))
        t = time.time()
        if self.sym_level != 0:
            initset_poly = initset  # pc.box2poly(initset.T)
            transform_information = cur_agent.get_transform_information(waypoint.mode_parameters,
                                                                        prev_waypoint.mode_parameters)  # initset_poly)
            initset_virtual = cur_agent.transform_poly_to_virtual(initset_poly, transform_information)
            bounded_initset_u: np.array = PolyUtils.get_bounding_box(initset_virtual)
        else:
            bounded_initset_u = PolyUtils.get_bounding_box(initset)
            transform_information = (0, 0)  # empty tuple just to return something
        # print("initset volume:", PolyUtils.get_rect_volume(PolyUtils.get_bounding_box(initset)))
        # print("initset_u volume:", PolyUtils.get_rect_volume(bounded_initset_u))
        transform_time += time.time() - t
        # print(bounded_initset_u)
        # TODO un-hardcode grid resolution
        grid_delta: np.array = grid_resolution
        # print("quantized_key_range: ", quantized_key_range)
        # print("initset_unified: ", bounded_initset_u)
        t = time.time()
        if self.sym_level != 0:
            abs_prev_waypoint = tuple(cur_agent.transform_mode_to_virtual(prev_waypoint.mode_parameters,
                                                                          transform_information))
            guard = PolyUtils.get_bounding_box(
                cur_agent.transform_poly_to_virtual(pc.box2poly(waypoint.original_guard.T),
                                                    transform_information))
        else:
            guard = waypoint.original_guard
        if not is_last_waypoint and self.sym_level != 0:
            transform_information2 = cur_agent.get_transform_information(next_waypoint.mode_parameters,
                                                                         waypoint.mode_parameters)
            abs_cur_waypoint = tuple(cur_agent.transform_mode_to_virtual(waypoint.mode_parameters,
                                                                         transform_information2))
            abs_cur_edge = tuple([abs_prev_waypoint, abs_cur_waypoint])
            if not abs_cur_edge in self.abs_edges_guards:
                print("edge not found???")
                pdb.set_trace()

        accumulated_tubes: List[List[pc.array]] = []
        accumulated_traces: List[np.array] = []
        transform_time += time.time() - t
        counter = 0
        min_index = math.inf
        max_index = 0
        next_initset_u = np.empty(bounded_initset_u.shape)
        is_first_tube = True
        if self.sym_level == 2:
            if abs_prev_waypoint in self.uncovered_rects_per_mode:
                # print("self.uncovered_rects_per_mode[abs_prev_waypoint]: ",
                #      self.uncovered_rects_per_mode[abs_prev_waypoint])
                self.uncovered_rects_per_mode[abs_prev_waypoint].append(bounded_initset_u)
            else:
                self.uncovered_rects_per_mode[abs_prev_waypoint] = [bounded_initset_u]

            bounded_initset_u = PolyUtils.get_convex_union(self.uncovered_rects_per_mode[abs_prev_waypoint])
            self.uncovered_rects_per_mode[abs_prev_waypoint] = [bounded_initset_u]
        print("bounded_initset_u after_volume: ", PolyUtils.get_rect_volume(bounded_initset_u))
        print("bounded_initset_u after: ", bounded_initset_u)
        list_of_init_sets = []
        if self.sym_level == 2:
            list_of_init_sets = self.uncovered_rects_per_mode[abs_prev_waypoint]
        else:
            list_of_init_sets = [bounded_initset_u]
        for initset_u in list_of_init_sets:
            quantized_key_range: np.array = np.floor(initset_u / grid_delta)
            curr_key: np.array = quantized_key_range[0, :]
            # accumulated_tubes: List[List[pc.Polytope]] = []
            while True:
                curr_initset: np.array = np.row_stack((curr_key * grid_delta, curr_key * grid_delta + grid_delta))
                if PolyUtils.do_rects_inter(curr_initset, initset_u):
                    hash_key = tuple(curr_key)
                    t = time.time()
                    if self.sym_level != 0:
                        transformed_waypoint = copy.copy(waypoint)
                        transformed_waypoint.mode_parameters = cur_agent.get_virtual_mode_parameters()
                        transform_time += time.time() - t
                    else:
                        transformed_waypoint = copy.copy(waypoint)
                    # if time_bound != 19.99:
                    #    pass
                    if hash_key not in self.tube_dict or self.sym_level == 0:
                        trace, tube = self.tube_computer.compute_tube(cur_agent, curr_initset, transformed_waypoint,
                                                                      time_step)

                        # TODO support trace cacheing
                        # print("It has been computed, not transformed")
                        trace = None
                        self.tube_dict[hash_key] = UniformTubeset(tube, trace)
                        if self.sym_level == 2:
                            if abs_prev_waypoint not in self.abs_per_virtual_mode_tube_counter:
                                self.abs_per_virtual_mode_tube_counter[abs_prev_waypoint] = 1
                            else:
                                self.abs_per_virtual_mode_tube_counter[abs_prev_waypoint] += 1
                        self.computed_counter += 1
                    elif len(self.tube_dict[hash_key].tube) < math.ceil(time_bound / time_step):
                        # pdb.set_trace()
                        transformed_waypoint.time_bound = (math.ceil(time_bound / time_step) - len(
                            self.tube_dict[hash_key].tube)) * time_step
                        trace, tube = self.tube_computer.compute_tube(cur_agent, self.tube_dict[hash_key].tube[-1],
                                                                      transformed_waypoint, time_step)

                        # TODO extend trace as well
                        saved_fraction = len(self.tube_dict[hash_key].tube) / math.ceil(time_bound / time_step)
                        self.saved_counter += saved_fraction
                        self.computed_counter += 1 - saved_fraction
                        self.tube_dict[hash_key].tube = np.concatenate((self.tube_dict[hash_key].tube, tube), axis=0)
                    else:
                        # print("it has been transformed, not computed")
                        self.saved_counter += 1

                    if is_last_waypoint or self.sym_level != 2:
                        curr_min_index, curr_max_index, curr_next_initset_u = TubeMaster.intersect_waypoint(
                            self.tube_dict[hash_key].tube, guard)
                    else:
                        """
                        _, _, guards_inter_list = TubeMaster.intersect_waypoint_list(
                            self.tube_dict[hash_key].tube,
                            self.abs_edges_guards[abs_cur_edge])
                        """
                        curr_min_index, curr_max_index, curr_next_initset_u = TubeMaster.intersect_waypoint(
                            self.tube_dict[hash_key].tube,
                            self.abs_edges_guards_union[abs_cur_edge])
                        """
                        TAC deadline removed
                        # curr_next_initset_u = PolyUtils.get_convex_union([temp[0] for temp in guards_inter_list])
                        # curr_max_index = len(self.tube_dict[hash_key].tube) - 1
                        # curr_min_index = 0
    
                        for abs_waypoint in self.abs_nodes_neighbors[abs_prev_waypoint]:
                            abs_edge = tuple([abs_prev_waypoint, abs_waypoint])
                            if not abs_edge in self.abs_edges_guards:
                                print("edge not found???")
                                pdb.set_trace()
                            curr_min_index_temp, curr_max_index_temp, guards_inter_list = TubeMaster.intersect_waypoint_list(
                                self.tube_dict[hash_key].tube,
                                self.abs_edges_guards[abs_edge])
                            curr_min_index = min(curr_min_index, curr_min_index_temp)
                            curr_max_index = max(curr_max_index, curr_max_index_temp)
                        """

                    # print("curr_max_index: ", curr_max_index)
                    # print("tube_dict_length: ", len(self.tube_dict[hash_key].tube))
                    # self.tube_dict[hash_key].tube = self.tube_dict[hash_key].tube[:curr_max_index + 1]
                    used_tube = self.tube_dict[hash_key].tube  # [:curr_max_index + 1]
                    # print("used_tube_length: ", len(used_tube))
                    if self.tube_dict[hash_key].trace is None:
                        self.tube_dict[hash_key].trace = None
                    else:
                        self.tube_dict[hash_key].trace = self.tube_dict[hash_key].trace[:curr_max_index + 1]
                    if is_first_tube:
                        min_index = curr_min_index
                        max_index = curr_max_index
                        next_initset_u = np.copy(curr_next_initset_u)
                        is_first_tube = False
                    else:
                        min_index = min(min_index, curr_min_index)
                        max_index = max(max_index, curr_max_index)
                        next_initset_u = PolyUtils.get_convex_union([next_initset_u, curr_next_initset_u])
                    accumulated_tubes.append(used_tube)
                    accumulated_traces.append(self.tube_dict[hash_key].trace)
                    counter += 1
                if np.all(curr_key == quantized_key_range[1, :]):
                    break
                curr_key = next_quantized_key(curr_key, quantized_key_range)
                if self.sym_level == 2:
                    self.abs_initset_rects[abs_prev_waypoint].append(initset_u)

        if self.sym_level == 2:
            self.uncovered_rects_per_mode[abs_prev_waypoint] = []
        # TubeMaster.plot_segments([self.tube_dict[key].tube for key in self.tube_dict], True)
        # print("combining all tubes")
        # final_tube: List[pc.Polytope] = PolyUtils.merge_tubes(accumulated_tubes)
        # print("It returned!", self.computed_counter, "tubes computed")
        # print("It returned!", self.saved_counter, "tubes saved")
        # self.computed_counter = 0
        # self.saved_counter = 0
        final_tube: List[np.array] = PolyUtils.merge_tubes(accumulated_tubes)
        ##for i in range(len(final_tube)):
        #   self.tube_union = pc.union(self.tube_union, pc.box2poly(final_tube[i][:, :].T))
        # self.tube_dict[hash_key].tube

        """
        possible_next_initset_poly = pc.Region(list_poly=[])
        tube_inter_all_guards_poly = pc.box2poly(
            TubeMaster.intersect_waypoint_list(
                final_tube,
                self.trans_guards_bbox_list).T)
        for i in range(len(self.trans_info_list)):
            for j in range(1, len(self.trans_info_list[i])):
                possible_next_initset_poly = pc.union(possible_next_initset_poly,
                                                      cur_agent.transform_poly_to_virtual(
                                                          cur_agent.transform_poly_from_virtual(
                                                              tube_inter_all_guards_poly,
                                                              self.trans_info_list[
                                                                  i][j - 1]), self.trans_info_list[
                                                              i][j]))
        self.initset_union = pc.union(self.initset_union, initset_virtual)  # pc.box2poly(curr_initset.T)
        if not pc.is_subset(possible_next_initset_poly, self.initset_union):
            self.uncovered_sets = pc.union(self.uncovered_sets, possible_next_initset_poly)
            print("new initial set NOT covered")
        else:
            print("new initial set covered already")
        """

        # Checking for fixed point!!
        if self.sym_level == 2:
            for abs_waypoint in self.abs_nodes_neighbors[abs_prev_waypoint]:
                abs_edge = tuple([abs_prev_waypoint, abs_waypoint])
                if not abs_edge in self.abs_edges_guards:
                    print("edge not found???")
                    pdb.set_trace()
                """
                TAC deadline removed
                _, _, guards_inter_list = TubeMaster.intersect_waypoint_list(
                    final_tube,
                    self.abs_edges_guards[abs_edge])
                #########
                possible_next_initset_u = PolyUtils.get_convex_union([temp[0] for temp in guards_inter_list])
                possible_next_initset_rect = PolyUtils.get_bounding_box(cur_agent.transform_poly_to_virtual(
                    cur_agent.transform_poly_from_virtual(
                        pc.box2poly(possible_next_initset_u.T),
                        guards_inter_list[0][1]), guards_inter_list[0][2]))
                """
                _, max_inter_index, possible_next_initset_u = TubeMaster.intersect_waypoint(
                    final_tube,
                    self.abs_edges_guards_union[abs_edge])

                possible_next_initset_reg = pc.Region(list_poly=[])
                for i in range(len(self.abs_edges_guards[abs_edge])):
                    possible_next_initset_reg = pc.union(possible_next_initset_reg,
                                                         cur_agent.transform_poly_to_virtual(
                                                             cur_agent.transform_poly_from_virtual(
                                                                 pc.box2poly(possible_next_initset_u.T),
                                                                 self.abs_edges_guards[abs_edge][i][1]),
                                                             self.abs_edges_guards[abs_edge][i][2]))
                possible_next_initset_rect = PolyUtils.get_region_bounding_box(possible_next_initset_reg)
                possible_next_initset_reg = pc.box2poly(possible_next_initset_rect.T)
                if not pc.is_subset(possible_next_initset_reg, self.abs_initset[abs_waypoint]):
                    rect_contain = False
                    if abs_waypoint in self.abs_initset_rects:
                        for rect in self.abs_initset_rects[abs_waypoint]:
                            if PolyUtils.does_rect_contain(possible_next_initset_rect, rect):
                                rect_contain = True
                    if not rect_contain:
                        if not abs_waypoint in self.uncovered_sets_per_mode:
                            self.uncovered_sets_per_mode[abs_waypoint] = possible_next_initset_reg
                            self.uncovered_rects_per_mode[abs_waypoint] = [possible_next_initset_rect]
                        else:
                            self.uncovered_sets_per_mode[abs_waypoint] = pc.union(
                                self.uncovered_sets_per_mode[abs_waypoint],
                                possible_next_initset_reg)
                            self.uncovered_rects_per_mode[abs_waypoint].append(possible_next_initset_rect)
                ###########
                # possible_next_initset_reg = pc.Region(list_poly=[])
                # for j in range(len(guards_inter_list)):
                #     possible_next_initset_reg_temp = pc.box2poly(PolyUtils.get_bounding_box(cur_agent.transform_poly_to_virtual(
                #         cur_agent.transform_poly_from_virtual(
                #             pc.box2poly(guards_inter_list[j][0].T),
                #             guards_inter_list[j][1]), guards_inter_list[j][2])))
                #     possible_next_initset_reg = pc.union(possible_next_initset_reg,
                #                                          possible_next_initset_reg_temp)
                #     if not abs_waypoint in self.abs_initset:
                #         print("The virtual mode has no abs_initset???")
                #         pdb.set_trace()
                #     if not pc.is_subset(possible_next_initset_reg_temp, self.abs_initset[abs_waypoint]):
                #         if not abs_waypoint in self.uncovered_sets_per_mode:
                #             self.uncovered_sets_per_mode[abs_waypoint] = possible_next_initset_reg_temp
                #         else:
                #             self.uncovered_sets_per_mode[abs_waypoint] = pc.union(
                #                 self.uncovered_sets_per_mode[abs_waypoint],
                #                 possible_next_initset_reg_temp)
                # Hussein: the following is implementing the first method where tubes are shared between virtual modes
                if not pc.is_subset(possible_next_initset_reg, self.initset_union):
                    self.uncovered_sets = pc.union(self.uncovered_sets, possible_next_initset_reg)
        """
        possible_next_initset_reg = pc.Region(list_poly=[])
        for j in range(len(guards_inter_list)):
            possible_next_initset_reg_temp = cur_agent.transform_poly_to_virtual(
                cur_agent.transform_poly_from_virtual(
                    pc.box2poly(guards_inter_list[j][0].T),
                    guards_inter_list[j][1]), guards_inter_list[j][2])
            possible_next_initset_reg = pc.union(possible_next_initset_reg,
                                                 possible_next_initset_reg_temp)
            abs_waypoint = tuple(cur_agent.transform_mode_to_virtual(waypoint.mode_parameters,
                                                                     guards_inter_list[j][2]))
            if abs_waypoint not in self.abs_initset:
                print("This waypoint is the last waypoint in a path")
            if not pc.is_subset(possible_next_initset_reg_temp, self.abs_initset[abs_waypoint]):
                if not abs_waypoint in self.uncovered_sets_per_mode:
                    self.uncovered_sets_per_mode[abs_waypoint] = possible_next_initset_reg_temp
                else:
                    self.uncovered_sets_per_mode[abs_waypoint] = pc.union(self.uncovered_sets_per_mode[abs_waypoint],
                                                                          possible_next_initset_reg_temp)
        """
        if self.sym_level == 2:
            self.initset_union_rects.append(initset_u)
            # for i in range(initset_u.shape[1]):
            #    initset_u[0][i] -= 0.001
            #    initset_u[1][i] += 0.001
            initset_virtual = pc.box2poly(initset_u.T)
            self.initset_union = pc.union(self.initset_union, initset_virtual)  # pc.box2poly(curr_initset.T)

            if not abs_prev_waypoint in self.abs_initset or not abs_prev_waypoint in self.abs_initset_rects:
                print("Why??")
                pdb.set_trace()
            self.abs_initset[abs_prev_waypoint] = pc.union(self.abs_initset[abs_prev_waypoint], initset_virtual)
            # self.abs_initset_rects[abs_prev_waypoint].append(initset_u)

        if self.sym_level != 0:
            if not abs_prev_waypoint in self.abs_reach:
                print("Why??")
                pdb.set_trace()
            self.abs_reach[abs_prev_waypoint] = PolyUtils.merge_tubes(
                [self.abs_reach[abs_prev_waypoint], final_tube])  # [:max_index]

            # print("initset_union: ", self.initset_union)
            # print("new initial set NOT covered", possible_next_initset_reg)

            # self.initset_union = pc.reduce(self.initset_union)
            # self.uncovered_sets = pc.reduce(self.uncovered_sets)

        if self.sym_level == 2:
            new_uncovered_sets = pc.Region(list_poly=[])
            if type(self.uncovered_sets) == pc.Polytope:
                self.uncovered_sets = pc.Region(list_poly=[self.uncovered_sets])
            # print("len(self.uncovered_sets.list_poly) total : ", len(self.uncovered_sets.list_poly))
            for i in range(len(self.uncovered_sets.list_poly)):
                if not pc.is_subset(self.uncovered_sets.list_poly[i], self.initset_union):
                    new_uncovered_sets = pc.union(new_uncovered_sets, self.uncovered_sets.list_poly[i])
                    # print("previously uncovered set is still uncovered: ", self.uncovered_sets.list_poly[i])
                else:
                    pass
                    # print("previously uncovered set is currently covered")
            self.uncovered_sets = new_uncovered_sets  # pc.mldivide(self.uncovered_sets, initset_virtual)

        # Hussein: per mode checking for uncovered sets
        mode_num = 0

        fixed_point = False
        if self.sym_level == 2:
            fixed_point = True
            for virtual_mode in self.abs_initset:
                if not virtual_mode in self.uncovered_sets_per_mode:
                    self.uncovered_sets_per_mode[virtual_mode] = pc.Region(list_poly=[])
                if not virtual_mode in self.uncovered_rects_per_mode:
                    self.uncovered_rects_per_mode[virtual_mode] = []
                new_uncovered_sets = pc.Region(list_poly=[])
                if type(self.uncovered_sets_per_mode[virtual_mode]) == pc.Polytope:
                    self.uncovered_sets_per_mode[virtual_mode] = pc.Region(
                        list_poly=[self.uncovered_sets_per_mode[virtual_mode]])

                indices_to_delete = []

                for i in range(len(self.uncovered_rects_per_mode[virtual_mode])):
                    for init_rect in self.abs_initset_rects[virtual_mode]:
                        if PolyUtils.does_rect_contain(self.uncovered_rects_per_mode[virtual_mode][i], init_rect):
                            indices_to_delete.append(i)
                            break

                for index in sorted(indices_to_delete, reverse=True):
                    self.uncovered_rects_per_mode[virtual_mode].pop(index)

                for i in range(len(self.uncovered_sets_per_mode[virtual_mode].list_poly)):
                    if not pc.is_subset(self.uncovered_sets_per_mode[virtual_mode].list_poly[i],
                                        self.abs_initset[virtual_mode]):
                        new_uncovered_sets = pc.union(new_uncovered_sets,
                                                      self.uncovered_sets_per_mode[virtual_mode].list_poly[i])
                print("len(self.uncovered_sets.list_poly), ", mode_num, "th", " mode ", virtual_mode, " is: ",
                      len(self.uncovered_sets_per_mode[virtual_mode].list_poly))
                if len(self.uncovered_rects_per_mode[virtual_mode]) > 0:  # not pc.is_empty(new_uncovered_sets) and
                    fixed_point = False
                self.uncovered_sets_per_mode[virtual_mode] = new_uncovered_sets
                mode_num = mode_num + 1

            if fixed_point:
                print("FIXED POINT HAS BEEN REACHED!!!")

            # print("Bounding box of the uncovered_sets: ",
            #      PolyUtils.get_region_bounding_box(self.uncovered_sets))

            if pc.is_empty(self.uncovered_sets) and self.sym_level == 2:
                print("YEY! NO MORE CACHE MISSES")

        # assert all([not pc.is_subset(self.uncovered_sets.list_poly[i], initset_virtual) for i in range(len(self.uncovered_sets.list_poly))]), "wot"
        # for i in range(len(self.uncovered_sets), 0, -1):
        #    if pc.is_subset(pc.box2poly(self.uncovered_sets[i]), self.initset_union):
        #        self.uncovered_sets.pop(i)

        # if len(final_tube) == 0 or final_tube[0].dim == 0:
        #    pdb.set_trace()
        # print("final_tube, final_tube, final_tube,", final_tube)
        if len(final_tube) == 0 or final_tube[0][0, :].shape[0] == 0:
            pdb.set_trace()
        print("final_tube_initial_set: ", final_tube[0])
        print("final_tube_initial_set_volume: ", PolyUtils.get_rect_volume(final_tube[0]))
        # _, max_inter_index,_ = TubeMaster.intersect_waypoint(
        #    final_tube,
        #    guard)
        # final_tube = final_tube[:max_inter_index]
        # print("final tube:")
        # print(final_tube)
        # transformed_tubesegment = TubeMaster.transform_tubseg_from_unified(final_tube, waypoint.center, dir_angle)
        # TubeMaster.plot_segment(final_tube, True)
        # print("final_tube: ", final_tube)
        # print("min_index: ", min_index)
        # print("next_initset_u: ", next_initset_u)
        return final_tube, accumulated_traces, min_index, max_index, next_initset_u, transform_information, transform_time, fixed_point
